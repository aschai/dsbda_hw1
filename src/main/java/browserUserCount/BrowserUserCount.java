package browserUserCount;

import org.apache.hadoop.conf.Configuration;
import org.apache.hadoop.fs.FileSystem;
import org.apache.hadoop.fs.Path;
import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.SequenceFile;
import org.apache.hadoop.io.compress.SnappyCodec;
import org.apache.hadoop.mapreduce.Job;
import org.apache.hadoop.mapreduce.lib.input.FileInputFormat;
import org.apache.hadoop.mapreduce.lib.input.TextInputFormat;
import org.apache.hadoop.mapreduce.lib.output.FileOutputFormat;
import org.apache.hadoop.mapreduce.lib.output.SequenceFileOutputFormat;

import java.io.IOException;

/**
 * Класс для запуска задания для подсчета числа пользователей каждого браузера
 * на основе данных журнала веб-сервера
 */
public class BrowserUserCount {

    public static void main(String[] args) throws IOException,
            InterruptedException, ClassNotFoundException {

        Path inputPath = new Path(args[0]);
        Path outputDir = new Path(args[1]);

        // Создание конфигурации
        Configuration conf = new Configuration(true);

        // Создание задачи
        Job job = Job.getInstance(conf, "log request size");
        job.setJarByClass(BrowserUserCountMapper.class);

        // Настройка MapReduce
        job.setMapperClass(BrowserUserCountMapper.class);
        job.setCombinerClass(BrowserUserCountReducer.class);
        job.setReducerClass(BrowserUserCountReducer.class);
        job.setNumReduceTasks(4);

        // Задание типов ключей и значений
        job.setOutputKeyClass(BrowserWritable.class);
        job.setOutputValueClass(IntWritable.class);

        // Настройка получения входных данных
        FileInputFormat.addInputPath(job, inputPath);
        job.setInputFormatClass(TextInputFormat.class);

        // Настройка вывода
        FileOutputFormat.setOutputPath(job, outputDir);
        job.setOutputFormatClass(SequenceFileOutputFormat.class);
        FileOutputFormat.setCompressOutput(job, true);
        FileOutputFormat.setOutputCompressorClass(job, SnappyCodec.class);
        SequenceFileOutputFormat.setOutputCompressionType(job, SequenceFile.CompressionType.BLOCK);

        // Удаление старых выходных файлов
        FileSystem hdfs = FileSystem.get(conf);
        if (hdfs.exists(outputDir))
            hdfs.delete(outputDir, true);

        // Выполнение задачи
        int code = job.waitForCompletion(true) ? 0 : 1;
        System.exit(code);

    }

}
