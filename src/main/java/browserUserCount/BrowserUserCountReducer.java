package browserUserCount;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.mapreduce.Reducer;

import java.io.IOException;

/**
 * Reducer задания для подсчета числа пользователей каждого браузера
 * на основе данных журнала веб-сервера
 */
public class BrowserUserCountReducer extends Reducer<BrowserWritable, IntWritable, BrowserWritable, IntWritable> {

    public void reduce(BrowserWritable browser, Iterable<IntWritable> values, Context context)
            throws IOException, InterruptedException {
        int sum = 0;
        for (IntWritable value : values) {
            sum += value.get();
        }
        context.write(browser, new IntWritable(sum));
    }

}