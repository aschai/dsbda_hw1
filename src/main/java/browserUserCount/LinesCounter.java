package browserUserCount;

/**
 * Счетчики корректных и некорректных строк
 */
public enum LinesCounter {
    ValidLines,
    InvalidLines
}
