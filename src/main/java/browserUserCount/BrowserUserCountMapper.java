package browserUserCount;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapreduce.Mapper;

import java.io.IOException;

/**
 * Mapper задания для подсчета числа пользователей каждого браузера
 * на основе данных журнала веб-сервера
 */
public class BrowserUserCountMapper extends Mapper<Object, Text, BrowserWritable, IntWritable> {

    private final IntWritable ONE = new IntWritable(1);

    // Фрагменты строк, на основе которых по записи UserAgent определяется
    // браузер клиента
    private static final String FIREFOX_MUST_CONTAIN = "Firefox/";

    private static final String CHROME_MUST_CONTAIN = "Chrome/";

    private static final String SAFARI_MUST_CONTAIN = "Safari/";
    private static final String SAFARI_MUST_NOT_CONTAIN = "Chrome/";

    private static final String OPERA_MUST_CONTAIN = "Opera/";

    private static final String IE_MUST_CONTAIN = "; MSIE ";


    public void map(Object key, Text value, Context context)
            throws IOException, InterruptedException {

        //UserAgent - последнее значение в строке лога
        String[] properties = value.toString().split("\"");

        String userAgent = properties[properties.length-1];
        BrowserWritable browser;
        if (userAgent.contains(IE_MUST_CONTAIN))
            browser = new BrowserWritable(BrowserWritable.IE);
        else if (userAgent.contains(OPERA_MUST_CONTAIN))
            browser = new BrowserWritable(BrowserWritable.OPERA);
        else if (userAgent.contains(CHROME_MUST_CONTAIN))
            browser = new BrowserWritable(BrowserWritable.CHROME);
        else if (userAgent.contains(SAFARI_MUST_CONTAIN)
                && !userAgent.contains(SAFARI_MUST_NOT_CONTAIN))
            browser = new BrowserWritable(BrowserWritable.SAFARI);
        else if (userAgent.contains(FIREFOX_MUST_CONTAIN))
            browser = new BrowserWritable(BrowserWritable.FIREFOX);
        else
            browser = new BrowserWritable("Undefined Browser");
        if (properties.length != 6) {
            context.getCounter(LinesCounter.class.getName(), LinesCounter.InvalidLines.name()).increment(1);
        } else {
            context.write(browser, ONE);
            context.getCounter(LinesCounter.class.getName(), LinesCounter.ValidLines.name()).increment(1);
        }
    }
}