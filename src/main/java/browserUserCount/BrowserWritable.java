package browserUserCount;

import org.apache.hadoop.io.Writable;
import org.apache.hadoop.io.WritableComparable;

import java.io.DataInput;
import java.io.DataOutput;
import java.io.IOException;

/**
 * CustomType для представления названия браузера
 */
public class BrowserWritable implements Writable, WritableComparable {

    public static final String FIREFOX = "Mozilla Firefox";
    public static final String CHROME = "Chrome";
    public static final String SAFARI = "Safari";
    public static final String OPERA = "Opera";
    public static final String IE = "Internet Explorer";
    public static final String UNDEFINED = "Undefined Browser";

    public String browserName;

    public BrowserWritable(String browserName) { this.browserName = browserName; }

    public BrowserWritable() { this.browserName = UNDEFINED; }

    public void write(DataOutput dataOutput) throws IOException {
        dataOutput.writeUTF(browserName);
    }

    public void readFields(DataInput dataInput) throws IOException {
        browserName = dataInput.readUTF();
    }

    public int compareTo(Object o) {
        return ((BrowserWritable)o).browserName.compareTo(browserName);
    }

    public String toString() { return browserName; }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;

        BrowserWritable that = (BrowserWritable) o;

        return browserName != null ? browserName.equals(that.browserName) : that.browserName == null;
    }

    @Override
    public int hashCode() {
        return browserName != null ? browserName.hashCode() : 0;
    }
}
