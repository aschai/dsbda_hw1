package browserUserCount;

import org.apache.hadoop.io.IntWritable;
import org.apache.hadoop.io.LongWritable;
import org.apache.hadoop.io.Text;
import org.apache.hadoop.mapred.Counters;
import org.apache.hadoop.mapreduce.Counter;
import org.apache.hadoop.mapreduce.Mapper;
import org.junit.Assert;
import org.junit.Test;

import java.util.HashMap;
import java.util.Map;

import static org.mockito.Mockito.*;

//import org.mockito.Matchers;

/**
 * Unit-тест для класса new BrowserUserCountMapper
 */
public class BrowserUserCountMapperTest
{

    private static HashMap<Text, BrowserWritable> inputsToOutputs = new HashMap<Text, BrowserWritable>();

    private static HashMap<Text, Boolean> inputsToValidity = new HashMap<Text, Boolean>();

    static
    {
        inputsToOutputs.put(new Text("28.11.122.124 -- [18/10/2017:22:50:42 -0400] \"GET https://alpha.wallhaven.cc/wallpapers/thumb/small/th-571057.jpg HTTP/1.1\" 200 328 \"https://alpha.wallhaven.cc/toplist\" \"Mozilla/5.0 (X11; Ubuntu; Linux i686; rv:16.0) Gecko/20120815 Firefox/16.0\""), new BrowserWritable("Mozilla Firefox"));
        inputsToOutputs.put(new Text("98.164.50.237 -- [18/10/2017:22:50:42 -0400] \"GET http://httpd.apache.org/docs/1.3/logs HTTP/1.1\" 200 30661 \"http://httpd.apache.org/docs/1.3/logs#accesslog\" \"Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Trident/5.0)\""), new BrowserWritable("Internet Explorer"));
        inputsToOutputs.put(new Text("147.172.216.221 -- [18/10/2017:22:50:42 -0400] \"GET http://httpd.apache.org/docs/1.3/logs HTTP/1.1\" 200 7334 \"http://httpd.apache.org/docs/1.3/logs#accesslog\" \"Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_7; en-US) AppleWebKit/534.16 (KHTML, like Gecko) Chrome/10.0.648.205 Safari/534.16\""), new BrowserWritable("Chrome"));

        inputsToValidity.put(new Text("147.172.216.221 -- [18/10/2017:22:50:42 -0400] \"GET http://httpd.apache.org/docs/1.3/logs HTTP/1.1\" 200 7334 \"http://httpd.apache.org/docs/1.3/logs#accesslog\" \"\""), true);
        inputsToValidity.put(new Text("147.172.216.221 -- [18/10/2017:22:50:42 -0400] \"GET http://httpd.apache.org/docs/1.3/logs HTTP/1.1\" 200 7334 \"http://httpd.apache.org/docs/1.3/logs#accesslog\" Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_7; en-US) AppleWebKit/534.16 (KHTML, like Gecko) Chrome/10.0.648.205 Safari/534.16\""), false);
        inputsToValidity.put(new Text("147.172.216.221 -- [18/10/2017:22:50:42 -0400] \"GET http://httpd.apache.org/docs/1.3/logs HTTP/1.1\" 200 7334 \"http://httpd.apache.org/docs/1.3/logs#accesslog\" \"Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_7; en-US) AppleWebKit/534.16 (KHTML, like Gecko) Chrome/10.0.648.205 Safari/534.16\""), true);
        inputsToValidity.put(new Text("147.172.216.221 -- [18/10/2017:22:50:42 -0400] \"GET http://httpd.apache.org/docs/1.3/logs HTTP/1.1\" 200 7334"), false);
        inputsToValidity.put(new Text("147.172.216.221 -- [18/10/2017:22:50:42 -0400] \"GET http://httpd.apache.org/docs/1.3/logs HTTP/1.1\" 200 7334 \"http://httpd.apache.org/docs/1.3/logs#accesslog\" \"Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_7; en-US) AppleWebKit/534.16 (KHTML, like Gecko) Chrome/10.0.648.205"), false);
    }


//    /**
//     * Проверяет правильность действий, выполняемых для корректных и некорректных
//     * входных значений
//     */
//    @Test
//    public void testCountInvalidLinesCorrectly()
//    {
//        Mapper.Context context = mock(Mapper.Context.class);
//        Counter validCounter = mock(Counter.class);
//        Counter invalidCounter = mock(Counter.class);
//        when(context.getCounter(LinesCounter.class.getName(), LinesCounter.InvalidLines.name())).thenReturn(invalidCounter);
//        when(context.getCounter(LinesCounter.class.getName(), LinesCounter.ValidLines.name())).thenReturn(validCounter);
//        BrowserUserCountMapper mapper = new BrowserUserCountMapper();
//        for (Map.Entry<Text, Boolean> e : inputsToValidity.entrySet()) {
//            try {
//                mapper.map(new LongWritable(1), e.getKey(), context);
//                if (e.getValue()) {
//                    verify(context, times(1)).write(any(BrowserWritable.class), anyInt());
//                    verify(validCounter).increment(1);
//                } else {
//                    verify(invalidCounter).increment(1);
//                    verify(context, never()).write(any(BrowserWritable.class), anyInt());
//                }
//            } catch (Exception ex) {
//                ex.printStackTrace();
//                Assert.fail();
//            }
//        }
//    }

    /**
     * Проверяет правильность формирования выходных значений при передаче корректных данных
     */
    @Test
    public void testExtractBrowserCorrectly()
    {
        Mapper.Context context = mock(Mapper.Context.class);
        Counter counter = new Counters.Counter();
        when(context.getCounter(anyString(), anyString())).thenReturn(counter);
        BrowserUserCountMapper mapper = new BrowserUserCountMapper();
        for (Map.Entry<Text, BrowserWritable> e : inputsToOutputs.entrySet()) {
            try {
                mapper.map(new LongWritable(1), e.getKey(), context);
                verify(context).write(e.getValue(), new IntWritable(1));
            } catch (Exception ex) {
                ex.printStackTrace();
                Assert.fail();
            }
        }
    }
}
