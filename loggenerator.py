import datetime
import random


def main():
    num_lines = 500000
    valid_line_template = '{ip} -- [{timestamp}] \"GET {request} HTTP/1.1\" {status} {response_size} ' \
                    '\"{referer}\" \"{user_agent}\"'
    invalid_line_template = '{ip} -- [{timestamp}] \"GET {request} HTTP/1.1\" {status} {response_size} ' \
                    '\"{referer} \"{user_agent}\"'
    referers = [
        "http://host2/sun_ss5/",
        "http://www.stumbleupon.com/refer.php?url=http%3A%2F%host1%2Fsun_ss5%2F",
        "http://httpd.apache.org/docs/1.3/logs#accesslog",
        "https://alpha.wallhaven.cc/toplist",
        "https://mephi.ru/"
    ]
    requests = {
        "http://host2/sun_ss5/": ["/sun_ss5/pdf.gif"],
        "http://www.stumbleupon.com/refer.php?url=http%3A%2F%host1%2Fsun_ss5%2F": ["/sun_ss5/"],
        "http://httpd.apache.org/docs/1.3/logs#accesslog": ["http://httpd.apache.org/docs/1.3/logs"],
        "https://alpha.wallhaven.cc/toplist": [
            "https://alpha.wallhaven.cc/wallpapers/thumb/small/th-571057.jpg",
            "https://static.wallhaven.cc/images/layout/logo_sm.png"
        ],
        "https://mephi.ru/": ["https://home.mephi.ru/study_groups?term_id=5"]
    }
    user_agents = {
        "IE": "Mozilla/5.0 (compatible; MSIE 9.0; Windows NT 6.1; Trident/5.0)",
        "Chrome": "Mozilla/5.0 (Macintosh; U; Intel Mac OS X 10_6_7; en-US) AppleWebKit/534.16 "
                  "(KHTML, like Gecko) Chrome/10.0.648.205 Safari/534.16",
        "Firefox": "Mozilla/5.0 (X11; Ubuntu; Linux i686; rv:16.0) Gecko/20120815 Firefox/16.0",
        "Opera": "Opera/9.80 (Windows NT 6.1; U; ru) Presto/2.8.131 Version/11.10",
        "Safari": "Mozilla/5.0 (Macintosh; I; Intel Mac OS X 10_6_7; ru-ru) "
                  "AppleWebKit/534.31+ (KHTML, like Gecko) Version/5.0.5 Safari/533.21.1"
    }
    status = 200

    log = open("log_" + datetime.datetime.now().strftime('%d-%m-%Y_%H-%M-%S'), 'w')

    while num_lines > 0:
        timestamp = datetime.datetime.now().strftime('%d/%m/%Y:%H:%M:%S -0400')
        ip = str(random.randint(0, 255))
        ip += '.' + str(random.randint(0, 255))
        ip += '.' + str(random.randint(0, 255))
        ip += '.' + str(random.randint(0, 255))
        referer = random.choice(referers)
        request = random.choice(requests[referer])
        status = 200
        response_size = random.randint(0, 40028)
        user_agent = random.choice(list(user_agents.values()))

        if num_lines % 20 == 0:
            line_template = random.choice([invalid_line_template, valid_line_template])
        else:
            line_template = valid_line_template

        new_line = line_template.format(
            ip=ip,
            timestamp=timestamp,
            request=request,
            status=status,
            response_size=response_size,
            referer=referer,
            user_agent=user_agent)
        log.write(new_line + '\n')
        num_lines -= 1
        if num_lines % 1000 == 0:
            print("{} lines left...".format(num_lines))

    log.close()
    print('File was created successfully')


if __name__ == "__main__":
    main()
